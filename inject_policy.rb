#!/usr/bin/env ruby
$LOAD_PATH.unshift "/home/vagrant/mithril/lib"
require 'bundler'
require 'mithril'

def library_code(lib)
  section('.plt',lib)
  section('.text',lib)
end

def read_write_all()
  range(0, (1 << 47 - 1)) do
    read
    write
  end
end

Elf::rewrite(ARGV[0]) {|file|
  Elf::Policy.inject_symbols(file)
  x = Elf::Policy.build do

    tag :libc do
      library_code('libc.so.6')
      library_code('ld-linux-x86-64.so.2')
    end

    tag :plt do
      section('.plt')
      section('.text') # Has initializers, etc
    end

    # start
    tag :main do
      # section('mainsec')
      symbol 'main'
    end

    # filter
    tag :wpng_init_io do
      symbol 'wpng_init_io'
    end

    # target
    tag :png_init_io do
      symbol 'png_init_io'
    end

    # tag :count do
    #   section('.count')
    # end

    # tag :libhammer do
    #   library_code('libhammer.so')
    # end

    state 'main' do
      exec :libc
      exec :plt
      readwrite :default
      readwrite :count

      mem :main do
          exec
          write #This should really be write :got ,but we have overlap
      end

      to 'wpng_init_io' do
        call 'wpng_init_io'
      end

      to 'fail' do
        call_noreturn 'png_init_io'
      end

      to 'libc' do
        call '_dl_runtime_resolve'
      end
    end

    state 'libc' do
      exec :libc
      exec :plt
      readwrite :main # Should be write GOT, but oh well
      readwrite :default
      to 'main' do
        call section_start('.fini')
      end
    end

    state 'fail' do
    end

    state 'wpng_init_io' do
      exec :wpng_init_io
      exec :libc
      exec :plt
      readwrite :default
      to 'libc' do
        call '_dl_runtime_resolve'
      end
      to 'png_init_io' do
        call_noreturn 'png_init_io'
      end
    end

    # state 'libhammer' do
    #   exec :libc
    #   exec :plt
    #   readwrite :main
    #   readwrite :default
    #   to 'libc' do
    #     call '_dl_runtime_resolve'
    #   end
    # end

    start 'main'
  end
  x.inject(file)
}
