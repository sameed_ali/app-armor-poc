CC=gcc
CFLAGS=-falign-functions=4096 -I../libpng_exploit/zlib/ -I../libpng_exploit/libpng/ -fno-stack-protector -z execstack -O -g -DPNG_DEBUG=5 -m32 -L../libpng_exploit/libpng/ -L../libpng_exploit/zlib/ -lpng -lz -lm
# THIS SHOULD BE $(pkg-config --cflags --libs libhammer)
LIBHAMMER=-I/usr/local/include  -L/usr/local/lib -lhammer $(pkg-config --cflags --libs libhammer)

.DEFAULT_GOAL := readpng
.PHONY : pingpong clean

pingpong:
	$(CC) $(CFLAGS) pingpong.c lib.c -o $@

nolangsec-readpng: readpng.c
	$(CC) readpng.c $(CFLAGS) -o readppng_no_langsec

readpng: readpng.c langsec_readpng.c
	$(CC) -c readpng.c $(CFLAGS)
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
	$(CC) -c langsec_readpng.c $(CFLAGS) $(LIBHAMMER)
	objcopy readpng.o --redefine-sym png_create_read_struct=wpng_create_read_struct
	objcopy readpng.o --redefine-sym png_create_info_struct=wpng_create_info_struct
	objcopy readpng.o --redefine-sym png_destroy_read_struct=wpng_destroy_read_struct
	objcopy readpng.o --redefine-sym png_init_io=wpng_init_io
	objcopy readpng.o --redefine-sym png_set_sig_bytes=wpng_set_sig_bytes
	objcopy readpng.o --redefine-sym png_read_png=wpng_read_png
	ld -r readpng.o langsec_readpng.o -o without_libs.o -melf_i386
	$(CC) without_libs.o $(CFLAGS) $(LIBHAMMER) -o out.exe

gpof-readpng: readpng.c langsec_readpng.c
	$(CC) -c readpng.c $(CFLAGS)
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
	$(CC) -c langsec_readpng.c $(CFLAGS) $(LIBHAMMER)
	objcopy readpng.o --redefine-sym png_create_read_struct=wpng_create_read_struct
	objcopy readpng.o --redefine-sym png_create_info_struct=wpng_create_info_struct
	objcopy readpng.o --redefine-sym png_destroy_read_struct=wpng_destroy_read_struct
	objcopy readpng.o --redefine-sym png_init_io=wpng_init_io
	objcopy readpng.o --redefine-sym png_set_sig_bytes=wpng_set_sig_bytes
	objcopy readpng.o --redefine-sym png_read_png=wpng_read_png
	ld -r readpng.o langsec_readpng.o -o without_libs.o -melf_i386
	$(CC) -pg without_libs.o $(CFLAGS) $(LIBHAMMER) -o out.exe

#elfbac:
#	# -- langsec
#	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
#	# gcc run.c $(pkg-config --cflags --libs libhammer)
#	gcc -c pingpong.c lib.c langsec.c -g -ggdb -falign-functions=4096 -m32
#	objcopy langsec.o --redefine-sym pong=libpong
#	objcopy langsec.o --redefine-sym wpong=pong
#	objcopy lib.o --redefine-sym pong=libpong
#	gcc *.o -o pingpong_pol -m32
#	./inject_policy.rb pingpong_pol
#
#fail: pingpong
#	cp pingpong pingpong_pol_fail
#	# objcopy --add-symbol wpong=0 pingpong_pol_fail
#	./fail_policy.rb pingpong_pol_fail

clean:
	rm -rf *~ *.bak core pingpong pingpong_pol* *.o readpng readpng_no_langsec out.exe
