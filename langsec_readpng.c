#include <stdio.h>
#include <stdlib.h>
#include "png.h"
#include <hammer/hammer.h>
#include <hammer/glue.h>

// typedef struct myPNGChunk {
//     int type;
//     int size;
// } myPNGChunk_t;

/*
   png_structp PNGAPI
   (*wpng_create_read_struct)() = png_create_read_struct;
 */
png_structp PNGAPI
wpng_create_read_struct(png_const_charp user_png_ver, png_voidp error_ptr, png_error_ptr error_fn, png_error_ptr warn_fn) {
	printf("Inside langsec parser for png_create_read_struct\n");
	return png_create_read_struct(user_png_ver, error_ptr, error_fn, warn_fn);
}

/*
   png_infop PNGAPI
   (*wpng_create_info_struct)() = png_create_info_struct;
 */
png_infop PNGAPI
wpng_create_info_struct(png_structp png_ptr) {
	printf("inside langsec parser for png_create_info_struct\n");
	return png_create_info_struct(png_ptr);
}

/*
   void PNGAPI
   (*wpng_destroy_read_struct)() = png_destroy_read_struct;
 */
void PNGAPI
wpng_destroy_read_struct(png_structpp png_ptr_ptr, png_infopp info_ptr_ptr, png_infopp end_info_ptr_ptr) {
	printf("In langsec parser for png_destroy_read_struct\n");
	return png_destroy_read_struct(png_ptr_ptr, info_ptr_ptr, end_info_ptr_ptr);
}

HParsedToken* chunk_header_parse_action(const HParseResult *p, void *user_data) {
    uint8_t value = (p->ast->uint > '9') ? p->ast->uint - 'a' + 10
        : p->ast->uint - 'f';
    HParsedToken *t = H_MAKE_UINT(value);
    return t;
}

bool validate_chunk_len(HParseResult *p, void* user_data) {
  if (TT_UINT == p->ast->token_type) {
    int val = (int) p->ast->uint;
    if (val >= 0 && val <= 255) {
       return true;
    }
  }
  return false;
}

bool validate_chunk_type(HParseResult *p, void* user_data) {
  if (TT_SEQUENCE != p->ast->token_type) {
    return false;
  }
  return true;
}

HParser* get_PNG_parser() {
	const HParser* MAGIC = h_token("\x89\x50\x4E\x47\x0D\x0A\x1A\x0A", 8);
    const HParser* IHDR_len = h_int_range(h_uint32(), 13, 13);
    const HParser* IHDR_type = h_token("IHDR", 4);
    const HParser* IHDR_chunk = h_sequence(h_uint32(), h_uint32(), h_repeat_n(h_int8(), 5), NULL);
    const HParser* IHDR_crc = h_uint32();

    // const HParser* chunk_len = h_attr_bool(h_int32(), validate_chunk_len, NULL);
    // const HParser* chunk_type = h_attr_bool(h_repeat_n(h_ch_range(0x00, 0xFF), 4), validate_chunk_type, NULL);
    // const HParser* chunk_crc = h_int32();
    // const HParser* chunk = h_sequence(chunk_len, chunk_type, NULL);// chunk_body, chunk_crc, NULL);

    const HParser* byte = h_uint8();

    // tRNS chunk
    const HParser* tRNS_chunk_type = h_token("tRNS", 4);
    const HParser* tRNS_chunk_len = h_left(h_int_range(h_uint32(), 0, 255), tRNS_chunk_type);
    const HParser* tRNS_chunk = h_length_value(tRNS_chunk_len, byte);

    // any other chunk
    const HParser* chunk_type = h_not(h_token("tRNS", 4));
    const HParser* chunk_len = h_left(h_uint32(), chunk_type);
    const HParser* chunk = h_length_value(chunk_len, byte);

    const HParser* chunks = h_many1(chunk);

    HParser* PNG_parser = h_sequence(MAGIC, IHDR_len, IHDR_type, IHDR_chunk, IHDR_crc, chunks, NULL);
    return PNG_parser;
}

/*
   void PNGAPI
   (*wpng_init_io)() = png_init_io;
 */
void PNGAPI
wpng_init_io(png_structp png_ptr, png_FILE_p fp) {
	printf("In langsec parser for png_init_io\n");

	uint8_t input[3500];
	size_t inputsize = 3500;

	HParser* PNGparser = get_PNG_parser();
    printf("Reading input... %d\n", sizeof(input));

	inputsize = fread(input, 1, sizeof(input), fp);

    printf("parsing...\n");

	HParseResult *result = h_parse(PNGparser, input, inputsize);

	if(result) {
        h_pprint(stdout, result->ast, 0, 0);
		printf("LANGSEC: OK.\n");
	} else {
		printf("LANGSEC: parse failed!\n");
        exit(0);
	}

    // reset file pointer to beginning of file
    fseek(fp, 0, SEEK_SET);
	return png_init_io(png_ptr, fp);
}

/*
   void PNGAPI
   (*wpng_set_sig_bytes)() = png_set_sig_bytes;
 */
void PNGAPI
wpng_set_sig_bytes(png_structp png_ptr, int num_bytes) {
	printf("In langsec parser for png_set_sig_bytes\n");
	return png_set_sig_bytes(png_ptr, num_bytes);
}

/*
   void PNGAPI
   (*wpng_read_png)() = png_read_png;
 */
void PNGAPI
wpng_read_png(png_structp png_ptr, png_infop info_ptr, int transforms, voidp params) {
	printf("In langsec parser for png_read_png\n");
	return png_read_png(png_ptr, info_ptr, transforms, params);
}

