#!/usr/bin/env ruby
$LOAD_PATH.unshift "/home/vagrant/mithril/lib"
require 'bundler'
require 'mithril'

def library_code(lib)
  section('.plt',lib)
  section('.text',lib)
end

def read_write_all()
  range(0, (1 << 47 - 1)) do
    read
    write
  end
end

Elf::rewrite(ARGV[0]) {|file|
  Elf::Policy.inject_symbols(file)
  x = Elf::Policy.build do

    tag :libc do
      library_code('libc.so.6')
      library_code('ld-linux-x86-64.so.2')
    end

    tag :plt do
      section('.plt')
      section('.text') # Has initializers, etc      
    end

    tag :main do
      # section('mainsec')
      symbol 'main'
    end      

    tag :pong do
      symbol 'pong' # Or we could do section('.text')
    end

    tag :ping do
      symbol 'ping'
    end

    tag :count do
      section('.count')
    end

    # tag :wpong do
    #   # section('wpongsec') 
    #   symbol 'wpong'
    # end

    state 'main' do      
      # exec :ping
      # exec :wpong
      exec :pong
      exec :libc
      exec :plt
      readwrite :default      
      readwrite :count
      mem :main do 
          exec
          write #This should really be write :got ,but we have overlap
      end
      to 'ping' do
        call 'ping'
      end
    end

    state 'libc' do
      exec :libc
      exec :plt
      readwrite :main # Should be write GOT, but oh well
      readwrite :default
      to 'main' do
        call section_start('.fini')
      end
    end

    state 'pong' do
      exec :pong
      exec :plt
      readwrite :default
      readwrite :count
      to 'libc' do
        call '_dl_runtime_resolve'
      end
      to 'ping' do
        call_noreturn  'ping'
      end
    end

    state 'fail' do
    end

    state 'wrappong' do
      # exec :pong
      # exec :wpong
      exec :libc
      exec :plt
      readwrite :default 
      to 'libc' do
        call '_dl_runtime_resolve'
      end
      to 'pong' do
        call_noreturn 'pong'
      end
      to 'fail' do
        call_noreturn '__real_pong'
      end
    end

    state 'ping' do
      exec :ping
      exec :plt
      exec :libc
      readwrite :default 
      readwrite :count 
      # to 'wrappong' do
      #   call_noreturn 'wpong'
      # end
      to 'fail' do
        call_noreturn 'pong'
      end
      to 'libc' do
        call '_dl_runtime_resolve'
      end
    end
    start 'main'
  end
  x.inject(file)
}
