#include <stdio.h>

extern void pong() __attribute__((section("pongsec")));
void ping() __attribute__((section("pingsec")));
int main() __attribute__((section("mainsec")));

int count __attribute__((section(".count"))) = 2;

void ping(){
  // printf("In ping()...\n");
  pong();
}

int main(){
  printf("In main()...\n");
  ping();
  printf("exit main()...\n");
}
