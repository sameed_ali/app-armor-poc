This example shows two functions calling each other. The level of this recursion depends on a global variable, `count`. 

`make` produces two executables, one without (`pingpong`) and one with (`pingpong_pol`) an ELFbac policy. The default policy is written in `inject_policy.rb`. This policy allows the execution of the program.

`make fail` produces a third policy executable, `pingpong_pol_fail`. This policy denies the `pong` function read access to the global `count` variable, resulting in a segfault.